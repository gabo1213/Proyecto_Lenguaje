# -*- coding: utf-8 -*-
from unidecode import unidecode
import os, sys
from random import randint
import json
from Queue import Queue
from openpyxl import load_workbook
import logging
import threading
import time
from random import randint
from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from core.models import *

def Loggin(request):
    Template = 'index.html'
    return render(request,Template,{})

def anadir_tabla(request):
    Template = 'Administracion/Anadir.html'
    return render(request, Template, {})

def ver_tabla(request):
    Template = 'Ver/Tabla.html'
    liblanca = LiBlanca.objects.all()
    electronica = Electronica.objects.all()
    hogar = Hogar.objects.all()
    patio = Patio.objects.all()
    jugueteria = Jugueteria.objects.all()
    return render(request, Template, {"liblanca":liblanca,"electronica":electronica,"hogar":hogar,"patio":patio,"jugueteria":jugueteria,"message":0})

def precio_max(request):
    Template = 'Ver/Tabla.html'

    maxi = int(request.POST["searchTerm2"])

    liblanca = LiBlanca.objects.all()
    electronica = Electronica.objects.all()
    hogar = Hogar.objects.all()
    patio = Patio.objects.all()
    jugueteria = Jugueteria.objects.all()

    liblanca = filter(lambda x:x.precio < maxi,liblanca)
    electronica = filter(lambda x:x.precio < maxi,electronica)
    hogar = filter(lambda x:x.precio < maxi,hogar)
    patio = filter(lambda x:x.precio < maxi,patio)
    jugueteria = filter(lambda x:x.precio < maxi,jugueteria)

    val1 = any(x.precio < maxi for x in liblanca)
    val2 = any(x.precio < maxi for x in electronica)
    val3 = any(x.precio < maxi for x in hogar)
    val4 = any(x.precio < maxi for x in patio)
    val5 = any(x.precio < maxi for x in jugueteria)

    if(val1 or val2 or val3 or val4 or val5):
        return render(request, Template, {"liblanca":liblanca,"electronica":electronica,"hogar":hogar,"patio":patio,"jugueteria":jugueteria,"message":0})
    else:
        return render(request, Template, {'message':1})

def venta_total(request):
    liblanca = LiBlanca.objects.all()
    electronica = Electronica.objects.all()
    hogar = Hogar.objects.all()
    patio = Patio.objects.all()
    jugueteria = Jugueteria.objects.all()

    a = int(reduce(lambda x , y: x.precio + y.precio,liblanca))
    b = int(reduce(lambda x , y: x.precio + y.precio,electronica))
    c = int(reduce(lambda x , y: x.precio + y.precio,hogar))
    d = int(reduce(lambda x , y: x.precio + y.precio,patio))
    e = int(reduce(lambda x , y: x.precio + y.precio,jugueteria))

    total = a+b+c+d+e
    return JsonResponse({'total':total})

logging.basicConfig( level=logging.DEBUG,
    format='[%(levelname)s] - %(threadName)-10s : %(message)s')

def Organizar(cola,generador):
    while cola.empty() == False:
        producto = cola.get()
        sku = next(generador)
        # logging("Este es el tipo de producto: %s " % (producto[2]))
        if producto[2] == "Linea Blanca":
            try:
                producto_v = get_object_or_404(LiBlanca,model=producto[1])
                producto_v.stock += int(producto[3])
                producto_v.save()
            except:
                n_producto = LiBlanca.objects.create(name=producto[0],
                                                    model=producto[1],
                                                    stock=producto[3],
                                                    consumo=producto[4],
                                                    precio=producto[6],
                                                    sku2=sku)


                n_producto.save()

        elif producto[2] == "Electronica":
            try:
                producto_v = get_object_or_404(Electronica,model=producto[1])
                producto_v.stock += int(producto[3])
                producto_v.save()

            except:
                n_producto = Electronica.objects.create(name=producto[0],
                                                        model=producto[1],
                                                        stock=producto[3],
                                                        consumo=producto[4],
                                                        garantia=producto[5],
                                                        precio=producto[6],
                                                        sku2=sku)
                n_producto.save()

        elif producto[2] == "Hogar":
            try:
                producto_v = get_object_or_404(Hogar,model=producto[1])
                producto_v.stock += int(producto[3])
                producto_v.save()

            except:
                n_producto = Hogar.objects.create(name=producto[0],
                                              model=producto[1],
                                              stock=producto[3],
                                              garantia=producto[5],
                                              precio=producto[6],
                                              sku2=sku)

                n_producto.save()

        elif producto[2] == "Jugueteria":
            try:
                producto_v = get_object_or_404(Jugueteria,model=producto[1])
                producto_v.stock += int(producto[3])
                producto_v.save()
            
            except:    
                n_producto = Jugueteria.objects.create(name=producto[0],
                                                    model=producto[1],
                                                    stock=producto[3],
                                                    garantia=producto[5],
                                                    precio=producto[6],                                                    
                                                    sku2=sku)


                n_producto.save()

        elif producto[2] == "Patio":
            try:
                producto_v = get_object_or_404(Patio,model=producto[1])
                producto_v.stock += int(producto[3])
                producto_v.save()
            except:
                n_producto = Patio.objects.create(name=producto[0],
                                                  model=producto[1],
                                                  stock=producto[3],
                                                  precio=producto[6],
                                                  sku2=sku)

                n_producto.save()

def descuento(request):
    liblanca = LiBlanca.objects.all()
    electronica = Electronica.objects.all()
    hogar = Hogar.objects.all()
    patio = Patio.objects.all()
    jugueteria = Jugueteria.objects.all()
    descuento = int(request.POST["searchTerm1"])
    c = map(lambda x:(x.precio)-(x.precio*descuento/100),liblanca)
    d = map(lambda x:(x.precio)-(x.precio*descuento/100),electronica)
    e = map(lambda x:(x.precio)-(x.precio*descuento/100),hogar)
    f = map(lambda x:(x.precio)-(x.precio*descuento/100),patio)
    g = map(lambda x:(x.precio)-(x.precio*descuento/100),jugueteria)

    cont = 0
    for i in liblanca:
        i.precio = c[cont]
        cont+=1
        i.save()
    cont = 0
    for i in electronica:
        i.precio = d[cont]
        cont+=1
        i.save()
    cont = 0
    for i in hogar:
        i.precio = e[cont]
        cont+=1
        i.save()
    cont = 0
    for i in patio:
        i.precio = f[cont]
        cont+=1
        i.save()
    cont = 0
    for i in jugueteria:
        i.precio = g[cont]
        cont+=1
        i.save()
    cont = 0

    Template = 'Ver/Tabla.html'
    return render(request, Template, {"liblanca":liblanca,"electronica":electronica,"hogar":hogar,"patio":patio,"jugueteria":jugueteria,"message":0})

def generador_codigo():
    dic = ["A","B","C","D","E","F","G","H","I","J"]
    while True:
        SKU = str(time.time()).strip(".")
        
        for num in SKU:
            if num == randint(0,9):
                i = int(num)
                num = dic[i]
        yield SKU


def LlenarStock(request):
    lista_objetos = []
    cola = Queue()
    lock = threading.Lock()
    productos = request.FILES['input-file-a']
    fs_productos = FileSystemStorage()
    filename_productos = fs_productos.save(productos.name.lower(), productos)
    productos_url = fs_productos.url(filename_productos)
    productos_url = productos_url[1:].capitalize()
    wb = load_workbook(productos_url)
    sheets_productos = []

    for sheet in wb.get_sheet_names():
        sheet = sheet.replace(u'\xa0', u'')
        sheet = str(sheet)
        sheets_productos.append(wb[sheet])


    for sheet in sheets_productos:
        i = 2
        while sheet["A"+str(i)].value != None:
            lista_objetos.append([unidecode(sheet["A"+str(i)].value),
                                  unidecode(sheet["B"+str(i)].value),
                                  unidecode(sheet["C"+str(i)].value),
                                  int(sheet["D"+str(i)].value),
                                  unidecode(str(sheet["E"+str(i)].value)),
                                  unidecode(sheet["F"+str(i)].value),
                                  int(sheet["G"+str(i)].value),

                                 ])
            i+=1

    for producto in lista_objetos:
        cola.put(producto)
    

    sku_gen = generador_codigo()

    os.remove(productos_url)
    hilos = []

    for i in range(1,5):
        hilo = threading.Thread(name="Ordenador %d"%(i),target=Organizar, args=(cola,sku_gen,))
        hilos.append(hilo)
        hilo.start()

    for hilo in hilos:
        print"Haciendo join"
        hilo.join()

    template = "index.html"
    return render(request,template,{})

def decorador(funcion):
    def nueva(*args):
        print "Bienvenido Profesor\n Este es Nuestro Proyecto de Lenguaje de Programacion"
        retorno = funcion(*args)
        return retorno
    return nueva

@decorador
def Bienvenido(request):
    Template = 'index.html'
    return render(request,Template,{'message':'Bienvenido Profesor','message2':'Este es Nuestro Proyecto de Lenguaje de Programacion'})