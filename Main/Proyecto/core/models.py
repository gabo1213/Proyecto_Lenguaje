# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class LiBlanca(models.Model):
    name = models.CharField(max_length=40)
    model = models.CharField(max_length = 40)
    stock = models.IntegerField()
    consumo = models.CharField(max_length = 40)
    precio = models.IntegerField(null=True,blank=True)
    sku2 = models.CharField(max_length = 40)
    def __str__(self):
        return "%s" % (self.model)

@python_2_unicode_compatible
class Electronica(models.Model):
    name = models.CharField(max_length=40)
    model = models.CharField(max_length = 40)
    stock = models.IntegerField()
    consumo = models.CharField(max_length = 40)
    garantia = models.CharField(max_length = 40)
    precio = models.IntegerField(null=True,blank=True)
    sku2 = models.CharField(max_length = 40)


    def __str__(self):
        return "%s" % (self.model)



@python_2_unicode_compatible
class Hogar(models.Model):
    name = models.CharField(max_length=40)
    model = models.CharField(max_length = 40)
    stock = models.IntegerField()
    garantia = models.CharField(max_length = 40)
    precio = models.IntegerField(null=True,blank=True)
    sku2 = models.CharField(max_length = 40)
    def __str__(self):
        return "%s" % (self.model)

@python_2_unicode_compatible
class Patio(models.Model):
    name = models.CharField(max_length=40)
    model = models.CharField(max_length = 40)
    stock = models.IntegerField()
    garantia = models.CharField(max_length = 40)
    precio = models.IntegerField(null=True,blank=True)
    sku2 = models.CharField(max_length = 40)
    def __str__(self):
        return "%s" % (self.model)

@python_2_unicode_compatible
class Jugueteria(models.Model):
    name = models.CharField(max_length=40)
    model = models.CharField(max_length = 40)
    stock = models.IntegerField()
    precio = models.IntegerField(null=True,blank=True)
    sku2 = models.CharField(max_length = 40)
    def __str__(self):
        return "%s" % (self.model)

# Create your models here.
