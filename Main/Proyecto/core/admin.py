# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from core.models import *

admin.site.register(LiBlanca)
admin.site.register(Electronica)
admin.site.register(Hogar)
admin.site.register(Patio)
admin.site.register(Jugueteria)

# Register your models here.
