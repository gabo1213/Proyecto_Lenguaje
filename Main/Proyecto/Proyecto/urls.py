"""Proyecto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from core import views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.Bienvenido,name='inicio'),
    url(r'^anadir_tabla/', views.anadir_tabla,name='anadir_tabla'),
    url(r'^ver_tabla/', views.ver_tabla,name='ver_tabla'),
    url(r'^Filtrar/', views.LlenarStock,name='Filtrar'),
    url(r'^descuento/', views.descuento,name='descuento'),
    url(r'^precio_max/', views.precio_max,name='precio_max'),
    url(r'^venta_total/', views.venta_total,name='venta_total'),




]
